const csvParser = require('csv-parser')
const fs = require('fs')

function matchesPerYear() {
  let { matchesPath, outputPath } = require('./index.cjs')
  let matchesPlayedPerSeason = {}
  let counter = 1

  fs.createReadStream(matchesPath)
    .pipe(csvParser())
    .on('data', (data) => {
      matchesPlayedPerSeason = countOccurence(data, matchesPlayedPerSeason)
    })
    .on('end', () => {
      console.log(JSON.stringify(matchesPlayedPerSeason))
      fs.createWriteStream(`${outputPath}1-matches-per-year.json`)
        .write(JSON.stringify(matchesPlayedPerSeason))
    });

  function countOccurence(data, matchesPlayedPerSeason) {
    if (Object.keys(matchesPlayedPerSeason).includes(data.season)) {
      matchesPlayedPerSeason[data.season]++
    }
    else {
      matchesPlayedPerSeason[data.season] = counter
    }
    return matchesPlayedPerSeason
  }
}

matchesPerYear()

module.exports = matchesPerYear