const csvParser = require('csv-parser')
const fs = require('fs')

function tossWinnerisMatchWinner() {

  let matchesWonWithTossWins = {}
  let counter = 1
  let { matchesPath, outputPath } = require('./index.cjs')

  fs.createReadStream(matchesPath)
    .pipe(csvParser())
    .on('data', (data) => {
      matchesWonWithTossWins = countWins(data, matchesWonWithTossWins)
    })
    .on('end', () => {
      console.log(JSON.stringify(matchesWonWithTossWins))
      fs.createWriteStream(`${outputPath}5-won-toss-won-match.json`)
        .write(JSON.stringify(matchesWonWithTossWins))
    })

  function countWins(data, matchesWonWithTossWins) {
    if (data.toss_winner == data.winner) {
      if (Object.keys(matchesWonWithTossWins).includes(data.winner)) {
        matchesWonWithTossWins[data.winner]++
      } else {
        matchesWonWithTossWins[data.winner] = counter
      }
    }
    return matchesWonWithTossWins
  }
}

tossWinnerisMatchWinner()

module.exports = tossWinnerisMatchWinner