const csvParser = require('csv-parser')
const fs = require('fs')

function menOfMatchs() {

    let playerOfMatchForEachSeason = {}
    let { matchesPath, outputPath } = require('./index.cjs')

    fs.createReadStream(matchesPath)
        .pipe(csvParser())
        .on('data', (data) => {
            if (Object.keys(playerOfMatchForEachSeason).includes(data.season)) {
                playerOfMatchForEachSeason[data.season] = countMOM(data, playerOfMatchForEachSeason[data.season])
            } else {
                playerOfMatchForEachSeason[data.season] = countMOM(data, {})
            }
        })
        .on('end', () => {
            playerOfMatchForEachSeason = compute(playerOfMatchForEachSeason)
            console.log(playerOfMatchForEachSeason)
            fs.createWriteStream(`${outputPath}6-player-of-match-for-each-season.json`)
                .write(JSON.stringify(playerOfMatchForEachSeason))
        })

    function countMOM(data, playerOfMatchForEachSeason) {
        if (Object.keys(playerOfMatchForEachSeason).includes(data.player_of_match)) {
            playerOfMatchForEachSeason[data.player_of_match]++
        } else {
            playerOfMatchForEachSeason[data.player_of_match] = 1
        }

        return playerOfMatchForEachSeason
    }

    function compute(playerOfMatchForEachSeason) {
        statsArr = Object.values(playerOfMatchForEachSeason)
        seasons = Object.keys(playerOfMatchForEachSeason)

        statsArr = statsArr.map(value => {
            return Object.fromEntries(Object.entries(value)
                .sort(([, player], [, playerOne]) => playerOne - player))
        })
        statsArr = statsArr.map(stat => Object.keys(stat)[0])
        statsArr = statsArr.reduce((result, key, index) => {
            result[seasons[index]] = key
            return result
        }, {})
        return (statsArr)
    }
}

menOfMatchs()

module.exports = menOfMatchs