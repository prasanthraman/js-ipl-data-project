const csvParser = require('csv-parser')
const fs = require('fs')

function highestBatsmanDismissal() {

    let batsmanBowler = {}
    let { deliveriesPath, outputPath } = require('./index.cjs')

    fs.createReadStream(deliveriesPath)
        .pipe(csvParser())
        .on('data', (data) => {
            if (data.player_dismissed != "" && data.dismissal_kind != "run out" && data.dismissal_kind != "retired hurt") {
                if (Object.keys(batsmanBowler).includes(data.player_dismissed)) {
                    batsmanBowler[data.player_dismissed] = compute(data, batsmanBowler[data.player_dismissed])
                }
                else {
                    batsmanBowler[data.player_dismissed] = compute(data, {})
                }
            }
        })
        .on('end', () => {
            batsmanBowler = computeHighest(batsmanBowler)
            console.log(JSON.stringify(batsmanBowler))
            fs.createWriteStream(`${outputPath}8-highest-dismissal-batsman-bowler.json`)
                .write(JSON.stringify(batsmanBowler))
        })

    function compute(data, batsmanBowler) {
        if (Object.keys(batsmanBowler).includes(data.bowler)) {
            batsmanBowler[data.bowler] += 1
        } else {
            batsmanBowler[data.bowler] = 1
        }
        return batsmanBowler
    }
    function computeHighest(batsmanBowler) {
        statsArr = Object.values(batsmanBowler)
        batsmen = Object.keys(batsmanBowler)

        statsArr = statsArr.map(value => {
            return Object.fromEntries(Object.entries(value)
                .sort(([, player], [, playerOne]) => playerOne - player))
        })
        statsArr = statsArr.map(stat => Object.keys(stat)[0])
        statsArr = statsArr.reduce((result, key, index) => {
            result[batsmen[index]] = key
            return result
        }, {})
        return (statsArr)
    }

}

highestBatsmanDismissal()

module.exports = highestBatsmanDismissal