const csvParser = require('csv-parser')
const fs = require('fs')

function highestEconomyInSuperOver() {

    let superBowler = {}
    let { deliveriesPath, outputPath } = require('./index.cjs')

    fs.createReadStream(deliveriesPath)
        .pipe(csvParser())
        .on('data', (data) => {
            if (data.is_super_over == "1") {
                if (Object.keys(superBowler).includes(data.bowler)) {
                    superBowler[data.bowler] = compute(data, superBowler[data.bowler])
                }
                else {
                    superBowler[data.bowler] = compute(data, [])
                }
            }
        })
        .on('end', () => {

            Object.keys(superBowler).reduce((result, key) => {
                console.log(superBowler[key])
                superBowler[key] = parseFloat(superBowler[key][0] / (superBowler[key][1] / 6)).toFixed(2)
                return result
            }, {})

            superBowler = Object.fromEntries(Object.entries(superBowler)
                .sort(([, player], [, playerOne]) => playerOne - player))
            console.log(superBowler)
            superBowler = `{"${Object.keys(superBowler)[0]}" :${Object.values(superBowler)[0]}}`
            console.log(JSON.stringify(superBowler))
            fs.createWriteStream(`${outputPath}9-player-with-highest-economy-in-superOver.json`)
                .write((superBowler))
        });

    function compute(data, superBowler) {
        if (superBowler.length == 0) {
            superBowler[0] = parseInt(data.total_runs)
            superBowler[1] = 1
        } else {
            superBowler[0] += parseInt(data.total_runs)
            superBowler[1] += 1
        }

        return superBowler
    }
}

highestEconomyInSuperOver()

module.exports = highestEconomyInSuperOver