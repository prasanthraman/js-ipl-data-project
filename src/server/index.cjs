let path=require('path')
let matchesPath=path.join(__dirname,'../data/matches.csv')
let deliveriesPath=path.join(__dirname,'../data/deliveries.csv')
let outputPath=path.join(__dirname,'../public/output/')


module.exports= {matchesPath,deliveriesPath,outputPath}

